import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class EditComment extends Component {

constructor(props) {
super(props);
this.state={
text: this.props.text,
};
}

render() {
return (
<form onSubmit={this.save.bind(this)}>
<div>
<input 
onChange={(e) => this.setState({text: e.target.value})}
defaultValue={this.props.text}
/>
</div>
<div>
<button type="submit">Сохранить</button>
<button onClick={() => this.cansel()}>Отмена</button>
</div>
</form>
);
}

save(e) {
this.props.onSave(this.state.text);
}

cansel() {
this.props.onCansel();
}
}

class Comment extends Component {

constructor(props) {
super(props);
this.state={
rat: props.rat,
edit: false,
text: props.text,
};
}

render() {
return (
<p key={this.props.id}>
<div>
<a href="#">Vadim Shmakov</a>
</div>
{this.getContent()}
</p>
);
}

getContent() {
if (this.state.edit) return (
<EditComment 
text={this.props.text}
onSave={this.saveHandler.bind(this)}
onCansel={() => this.setState({edit: false})}
/>
);

var id=this.props.id;
return (
<div>
<div>
<button onClick={() => this.props.onDislike(id)}>Dislike</button>
{this.props.rat}
<button onClick={() => this.props.onLike(id)}>Like</button>
</div>
<div>
{this.props.text}
</div>
<div>
<button onClick={() => this.edit()}>Редактировать</button>
</div>
</div>
);
}

saveHandler(t) {
this.props.onUpdate(t);
this.setState({
edit: false,
});
}

edit() {
this.setState({
edit: true,
});
}

}

class CommentInput extends Component {

constructor(props) {
super(props);
this.state={
text: "",
};
}

render() {
return (
<form onSubmit={this.createHandler.bind(this)}>
<input 
value={this.state.text}
onChange={this.changeHandler.bind(this)}/><br/>
<input type="submit" value="Отправить"/>
</form>
);
}

createHandler(e) {
e.preventDefault();
this.props.onCreate(this.state.text);
this.setState({
text: "",
});
}

changeHandler(e) {
this.setState({
text: e.target.value,
});
}
}

class App extends Component {

constructor(props) {
super(props);

this.state={
ord: "time",
comments: [],
};
}

  render() {
    return (
      <div className="comment-widget">
<div>
Отсортировать по:
<ul>
<li><button onClick={() => this.timeOrd()}>Времени добавления</button></li>
<li><button onClick={() => this.popOrd()}>Популярности</button></li>
</ul>
</div>

<CommentInput 
onCreate={this.createHandler.bind(this)}
/>
<div>
{this.getCommentsList()}
</div>
      </div>
    );
  }

getComments() {
return this.state.comments.slice();
}

setComments(c) {
this.setState({
comments: c,
});
}

createHandler(t) {
var c=this.getComments();
c.push({
id: c.reduce((id, c) => {
if (id <= c.id) id=c.id+1;
return id;
}, 1),
rat: 0,
text: t,
});
this.setComments(c);
return false;
}

ratHandler(id, clb) {
var comments=this.state.comments.map((c) => {
if (c.id == id) c.rat=clb(c.rat);
return c;
});
this.setComments(comments);
}

dislikeHandler(id) {
this.ratHandler(id, (r) => r-1);
}

likeHandler(id) {
this.ratHandler(id, (r) => r+1);
}

popOrd() {
var c=this.getComments();
c.sort((e1, e2) => {
return e1.rat > e2.rat ? -1 : 1;
});
this.setComments(c);
}

timeOrd() {
var c=this.getComments();
c.sort((e1, e2) => {
return e1.id < e2.id ? -1 : 1;
});
this.setComments(c);
}

updateHandler(id, t) {
this.setState({
comments: this.state.comments.map((c) => {
if (c.id == id) c.text=t;
return c;
}),
});
}

getCommentsList() {
var comments=this.state.comments.map((c) => {
return (
<Comment 
id={c.id} 
text={c.text} 
rat={c.rat}
onLike={this.likeHandler.bind(this)}
onDislike={this.dislikeHandler.bind(this)}
onUpdate={(t) => this.updateHandler(c.id, t)}
/>
);
});
return comments.length ? comments : (
<div>
Комментариев пока нет.
</div>
);
}
}

export default App;
